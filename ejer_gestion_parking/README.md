# Ejer_Gestion_Parking

## Pantalas de la aplicación

### Gestión del Parking
<img src="./img/Pantalla_Parking.PNG" width="725px"/>

### Sacar Cuentas
<img src="./img/Pantalla_Cuentas.PNG" width="725px"/>

### Gestion de Gastos
<img src="./img/Pantalla_Gastos.PNG" width="725px"/>

### Fijar Precio
<img src="./img/Pantalla_Precio.PNG" width="725px"/>