﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SacarCuentas
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.sc_datepicker_inicio = New System.Windows.Forms.DateTimePicker()
        Me.sc_datepicker_fin = New System.Windows.Forms.DateTimePicker()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.sc_input_gastos = New System.Windows.Forms.TextBox()
        Me.sc_input_ingresos = New System.Windows.Forms.TextBox()
        Me.sc_btn_aceptar = New System.Windows.Forms.Button()
        Me.sc_btn_cancelas = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(291, 40)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(169, 29)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Sacar Cuentas"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(188, 115)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(178, 16)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Fecha de Inicio de la cosulta"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(191, 182)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(167, 16)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Fecha de fin de la consulta"
        '
        'sc_datepicker_inicio
        '
        Me.sc_datepicker_inicio.Location = New System.Drawing.Point(296, 145)
        Me.sc_datepicker_inicio.Name = "sc_datepicker_inicio"
        Me.sc_datepicker_inicio.Size = New System.Drawing.Size(274, 20)
        Me.sc_datepicker_inicio.TabIndex = 3
        '
        'sc_datepicker_fin
        '
        Me.sc_datepicker_fin.Location = New System.Drawing.Point(296, 220)
        Me.sc_datepicker_fin.Name = "sc_datepicker_fin"
        Me.sc_datepicker_fin.Size = New System.Drawing.Size(274, 20)
        Me.sc_datepicker_fin.TabIndex = 4
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(152, 285)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(60, 16)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "Ingresos"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(553, 285)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(51, 16)
        Me.Label5.TabIndex = 6
        Me.Label5.Text = "Gastos"
        '
        'sc_input_gastos
        '
        Me.sc_input_gastos.Location = New System.Drawing.Point(556, 324)
        Me.sc_input_gastos.Name = "sc_input_gastos"
        Me.sc_input_gastos.Size = New System.Drawing.Size(175, 20)
        Me.sc_input_gastos.TabIndex = 7
        '
        'sc_input_ingresos
        '
        Me.sc_input_ingresos.Location = New System.Drawing.Point(155, 324)
        Me.sc_input_ingresos.Name = "sc_input_ingresos"
        Me.sc_input_ingresos.Size = New System.Drawing.Size(175, 20)
        Me.sc_input_ingresos.TabIndex = 8
        '
        'sc_btn_aceptar
        '
        Me.sc_btn_aceptar.Location = New System.Drawing.Point(469, 392)
        Me.sc_btn_aceptar.Name = "sc_btn_aceptar"
        Me.sc_btn_aceptar.Size = New System.Drawing.Size(75, 23)
        Me.sc_btn_aceptar.TabIndex = 9
        Me.sc_btn_aceptar.Text = "Aceptar"
        Me.sc_btn_aceptar.UseVisualStyleBackColor = True
        '
        'sc_btn_cancelas
        '
        Me.sc_btn_cancelas.Location = New System.Drawing.Point(323, 392)
        Me.sc_btn_cancelas.Name = "sc_btn_cancelas"
        Me.sc_btn_cancelas.Size = New System.Drawing.Size(75, 23)
        Me.sc_btn_cancelas.TabIndex = 10
        Me.sc_btn_cancelas.Text = "Cancelar"
        Me.sc_btn_cancelas.UseVisualStyleBackColor = True
        '
        'SacarCuentas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.sc_btn_cancelas)
        Me.Controls.Add(Me.sc_btn_aceptar)
        Me.Controls.Add(Me.sc_input_ingresos)
        Me.Controls.Add(Me.sc_input_gastos)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.sc_datepicker_fin)
        Me.Controls.Add(Me.sc_datepicker_inicio)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Name = "SacarCuentas"
        Me.Text = "SacarCuentas"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents sc_datepicker_inicio As DateTimePicker
    Friend WithEvents sc_datepicker_fin As DateTimePicker
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents sc_input_gastos As TextBox
    Friend WithEvents sc_input_ingresos As TextBox
    Friend WithEvents sc_btn_aceptar As Button
    Friend WithEvents sc_btn_cancelas As Button
End Class
