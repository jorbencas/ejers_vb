﻿Public Class Class1
    Private A As Double
    Private b As Double
    Private C As Double
    Private X As Double

    Public Sub New(a As Double, b As Double, c As Double)
        Me.A = a
        Me.b = b
        Me.C = c
    End Sub

    Public Sub New()
        Me.A = 0
        Me.b = 0
        Me.C = 0
    End Sub

    Public Sub ObtenerRaiz(resultado As Double)
        Console.WriteLine("Resultado" & resultado)
    End Sub

    Public Sub ObtenerRaices(resultado As Double, resultado2 As Double)
        Console.WriteLine("Resultado 1" & resultado & "Resultado 2" & resultado2)
    End Sub

    Public Function GetDiscriminante()
        Dim result As Double
        result = ((Me.b ^ 2) - 4 * Me.A * Me.b)
        Return TieneRaiz(result)
    End Function

    Public Function TieneRaiz(result As Double)
        If (result < 0) Then
            Return False
        ElseIf (result = 0) Then
            Return True
        End If
    End Function

    Public Function Calcular()
        Dim resultado As Double
        Dim resultado2 As Double

        If (GetDiscriminante().Equals(True)) Then 'Comprobem que el resultat de dircriminate es 0 o major
            resultado = ((-Me.b + Math.Sqrt((Me.b ^ 2) - 4 * Me.C)) / 2 * Me.A)
            resultado2 = ((-Me.b - Math.Sqrt((Me.b ^ 2) - 4 * Me.C)) / 2 * Me.A)
            ObtenerRaices(resultado, resultado2) 'Si hi han 2 posibles solucionesn imprimrem les 2
        Else
            resultado = (-Me.b / 2 * Me.A)
            ObtenerRaiz(resultado) 'Imprimim el resultat en cas de que nomes ni aguera 1
        End If
        Return resultado
    End Function

End Class
