﻿Module Module1

    Sub Main()
        Dim Vehiculos(3) As Parking
        Dim opcion, opt As Integer

        Do
            opt = Menu(opcion, Vehiculos)
        Loop Until opt = 4

    End Sub
    Public Function Menu(opcion As Integer, Vehiculos() As Parking)
        Console.Clear()
        Console.WriteLine("1. Introduce un alumno:")
        Console.WriteLine("2. Borra un alumno:")
        Console.WriteLine("3: Lista los alumno:")
        Console.WriteLine("4: Salir")
        opcion = Console.ReadLine()

        Select Case opcion
            Case 1
                opcion = 1
                Vehiculos = Añadiralumno(Vehiculos)
                escribirSecuencial(Vehiculos)
            Case 2
                opcion = 2
                BorrarVehiculos(Vehiculos)
            Case 3
                opcion = 3
                Vehiculos = leerSecuencial(Vehiculos)
                MostarTodos(Vehiculos)
                Console.Read()
            Case 4
                opcion = 4
                Exit Select
            Case Else
                Console.WriteLine("Opción incorrecta")
        End Select
        Return opcion
    End Function

    Public Sub MostarTodos(Vehiculos() As Parking)
        For contador = 0 To Vehiculos.Length - 1
            If (Vehiculos(contador).ocupado = True) Then
                Console.WriteLine("Matricula: " & Vehiculos(contador).Matricula)
                Console.WriteLine("Nombre del propietario: " & Vehiculos(contador).nombre_propetiario)
                Console.WriteLine("Apellido del propietario: " & Vehiculos(contador).apellidos_propietario)
                Console.WriteLine("Fecha: " & Vehiculos(contador).fecha)
                Console.WriteLine("Hora: " & Vehiculos(contador).hora)
            End If
        Next
    End Sub

    Public Sub BorrarVehiculos(Vehiculos() As Parking)
        Dim contador As Integer
        Dim numero As Integer

        Console.WriteLine("Introduce la matricula: ")
        numero = Convert.ToString(Console.ReadLine())

        For contador = 0 To Vehiculos.Length - 1
            If (Vehiculos(contador).Matricula = numero And Vehiculos(contador).ocupado = True) Then
                Vehiculos(contador).ocupado = False
            Else
                Console.WriteLine("Error, No hay plazas ocupados!!!!")
            End If
            Exit For
        Next
    End Sub

    Public Function Añadiralumno(Vehiculos() As Parking)
        Dim contador As Integer

        For contador = 0 To Vehiculos.Length - 1
            If (Vehiculos(contador).ocupado = False) Then
                Console.WriteLine("Introduce la matricula:")
                Vehiculos(contador).Matricula = Convert.ToString(Console.ReadLine())

                Console.WriteLine("Introduce el nombre:")
                Vehiculos(contador).nombre_propetiario = Convert.ToString(Console.ReadLine())

                Console.WriteLine("Introduce el Apellido:")
                Vehiculos(contador).apellidos_propietario = Convert.ToString(Console.ReadLine())

                Vehiculos(contador).fecha = Now.Date.ToString
                Vehiculos(contador).hora = Now.Hour.ToString
                Vehiculos(contador).ocupado = True
                Exit For
            End If
        Next
        Return Vehiculos
    End Function

    Public Sub escribirSecuencial(Vehiculos() As Parking)
        Dim Canal As Int16 = FreeFile()
        Dim Nombre As String = "../../vehiculos.txt"
        Dim Valor As String = ""
        Dim resultado As String = ""

        FileOpen(Canal, Nombre, OpenMode.Output)
        For contador = 0 To Vehiculos.Length - 1
            WriteLine(Canal, Convert.ToString(Vehiculos(contador).Matricula))
            WriteLine(Canal, Convert.ToString(Vehiculos(contador).nombre_propetiario))
            WriteLine(Canal, Convert.ToString(Vehiculos(contador).apellidos_propietario))
            WriteLine(Canal, Convert.ToString(Vehiculos(contador).fecha))
            WriteLine(Canal, Convert.ToString(Vehiculos(contador).hora))
            WriteLine(Canal, Convert.ToString(Vehiculos(contador).ocupado))
        Next
        FileClose(Canal)

    End Sub

    Public Function leerSecuencial(Vehiculos() As Parking)
        Dim Canal As Int16 = FreeFile()
        Dim Nombre As String = "../../vehiculos.txt"
        Dim contador As Integer = 0
        Dim contadorPos As Integer = 0
        FileOpen(Canal, Nombre, OpenMode.Input)
        Do While Not EOF(Canal)
            Dim DatosLeidos As String = ""
            Input(Canal, DatosLeidos)
            If (contadorPos < 3) Then
                Select Case contador
                    Case 0
                        Vehiculos(contadorPos).Matricula = DatosLeidos
                        contador = contador + 1

                    Case 1
                        Vehiculos(contadorPos).nombre_propetiario = DatosLeidos
                        contador = contador + 1

                    Case 2
                        Vehiculos(contadorPos).apellidos_propietario = DatosLeidos
                        contador = contador + 1
                    Case 3
                        Vehiculos(contadorPos).fecha = DatosLeidos
                        contador = contador + 1
                    Case 4
                        Vehiculos(contadorPos).hora = DatosLeidos
                        contador = contador + 1
                    Case 5

                        If (DatosLeidos = "True") Then
                            DatosLeidos = True
                        Else
                            DatosLeidos = False
                        End If

                        Vehiculos(contadorPos).ocupado = Convert.ToBoolean(DatosLeidos)
                        contadorPos = contadorPos + 1
                        contador = 0
                End Select
            End If


        Loop
        FileClose(Canal)
        Return Vehiculos
    End Function

    Public Structure Parking
        Public Matricula As String
        Public nombre_propetiario As String
        Public apellidos_propietario As String
        Public fecha As Date
        Public hora As String
        Public ocupado As Boolean
    End Structure
End Module

