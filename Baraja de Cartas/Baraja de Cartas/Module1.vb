﻿Module Module1
    Sub Main()
        Dim Baraja As New Baraja(GenerarBaraja())
        Console.WriteLine(Baraja.lenght)
        Console.ReadLine()

        Baraja.barajar()
        Baraja.imprimirCartas()
        Console.ReadLine()

        Dim carta As Carta = Baraja.siguienteCarta()
        Console.WriteLine("La siguiente carta es " & carta.ToString())
        Console.ReadLine()

        Console.WriteLine("Aun quedar " & Baraja.numeroDeCartasReparticles() & " cartas por repartir")
        Console.ReadLine()

        Dim listaCartasDevueltas As New List(Of Carta)
        listaCartasDevueltas = Baraja.darCartas(5)

        For Each carta In listaCartasDevueltas
            Console.WriteLine(carta.ToString())
        Next
        Console.WriteLine("Aun quedar " & Baraja.numeroDeCartasReparticles() & " cartas por repartir")
        Console.ReadLine()

        Baraja.CartasMonton()
        Console.ReadLine()

        Baraja.mostrarMonton()
        Console.ReadLine()
    End Sub

    Private Function GenerarBaraja()
        Dim palo() As String = {"Espadas", "Bastos", "Oros", "Copas"}
        Dim Cart As Carta
        Dim listaCartasDevueltas As New List(Of Carta)

        For Palos = 0 To palo.Length
            For Contador = 1 To 12
                If (Not (Contador.Equals(8) Or Contador.Equals(9))) Then
                    Cart = New Carta(Contador, palo(Palos))
                    listaCartasDevueltas.Add(Cart)
                End If
            Next
        Next

        Return listaCartasDevueltas
    End Function



End Module
