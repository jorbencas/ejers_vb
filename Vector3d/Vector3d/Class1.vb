﻿Option Explicit On
Option Strict On

Imports System.Math

Public Class Vector3D
    Implements IComparable(Of Vector3D)

    Private _x As Single
    Private _y As Single
    Private _z As Single


    Public Sub New(ByVal x As Single, ByVal y As Single, ByVal z As Single)
        Me._x = x
        Me._y = y
        Me._z = z
    End Sub

    Public Property X() As Single
        Get
            Return Me._x
        End Get
        Set(value As Single)
            Me._x = value
        End Set
    End Property

    Public Property Y() As Single
        Get
            Return Me._y
        End Get
        Set(value As Single)
            Me._y = value
        End Set
    End Property


    Public Property Z() As Single
        Get
            Return Me._z
        End Get
        Set(value As Single)
            Me._z = value
        End Set
    End Property

    Public Function Igual(ByVal otro As Vector3D) As Boolean
        Dim salida As Boolean
        salida = False

        If (Me._x = otro.X AndAlso Me._y = otro.Y AndAlso Me._z = otro.Z) Then
            salida = True
        Else
            salida = False
        End If

        Return salida

    End Function

    Public Function NormaMax() As Single
        Return CSng((Sqrt(Me._x ^ 2 + Me._y ^ 2 + Me._z ^ 2)))
    End Function

    Public Sub Dispose()
        Me._x = 0
        Me._y = 0
        Me._z = 0
    End Sub

    Protected Overrides Sub finalize()
        Me._x = 0
        Me._y = 0
        Me._z = 0
    End Sub

    Public Function CompareTo(ByVal Obj2 As Vector3D) As Integer Implements IComparable(Of Vector3D).CompareTo
        If Me._x.CompareTo(Obj2._x) <> 0 Then
            Return Me._x.CompareTo(Obj2._x)
        ElseIf Me._y.CompareTo(Obj2._y) <> 0 Then
            Return Me._y.CompareTo(Obj2._y)
        ElseIf Me.Z.CompareTo(Obj2._z) <> 0 Then
            Return Me._z.CompareTo(Obj2._z)
        Else
            Return 0
        End If
    End Function
End Class
