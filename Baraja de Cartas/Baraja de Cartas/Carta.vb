﻿Public Class Carta
    Private numero As Integer
    Private palo As String

    Public Sub New(numero As Integer, palo As String)
        Me.numero = numero
        Me.palo = palo
    End Sub

    Public Property Numero1 As Integer
        Get
            Return numero
        End Get
        Set(value As Integer)
            numero = value
        End Set
    End Property

    Public Property Palo1 As String
        Get
            Return palo
        End Get
        Set(value As String)
            palo = value
        End Set
    End Property

    Public Overrides Function ToString() As String
        Return "Numero: " & Me.numero & " Palo: " & Me.palo
    End Function
End Class
