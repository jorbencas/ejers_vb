﻿Public Class Persona
    Private _nombre As String
    Private _edad As Integer
    Private _dni As String
    Private _sexo As Char
    Private _altura As Integer
    Private _peso As Integer

    Public ReadOnly Property GetNombre() As String
        Get
            Return Me._nombre
        End Get
    End Property

    Public WriteOnly Property Nombre() As String
        Set(ByVal value As String)
            _nombre = value
        End Set
    End Property

    Public Property Edad() As Integer
        Get
            Return _edad
        End Get
        Set(ByVal value As Integer)
            _edad = value
        End Set
    End Property

    Public Property Dni() As String
        Get
            Return _dni
        End Get
        Set(ByVal value As String)
            _dni = value
        End Set
    End Property

    Public Property Sexo() As Char
        Get
            Return _sexo
        End Get
        Set(ByVal value As Char)
            _sexo = value
        End Set
    End Property

    Public Property Altura() As Integer
        Get
            Return _altura
        End Get
        Set(ByVal value As Integer)
            _altura = value
        End Set
    End Property

    Public Property Peso() As Integer
        Get
            Return _peso
        End Get
        Set(ByVal value As Integer)
            _peso = value
        End Set
    End Property

    Public Property Nombre1 As String
        Get
            Return _nombre
        End Get
        Set(value As String)
            _nombre = value
        End Set
    End Property

    Public Sub New()
        Me._nombre = ""
        Me._edad = 0
        Me._dni = "48608737P"
        Me._sexo = 'h'
        Me._altura = 0
        Me._peso = 0
    End Sub

    Public Sub New(ByVal nombre As String, ByVal edad As Integer, ByVal sexo As Char)
        Me._nombre = nombre
        Me._edad = edad
        Me._sexo = sexo
    End Sub

    Public Sub New(ByVal nombre As String, ByVal edad As Integer, ByVal sexo As Char, ByVal altura As Integer, ByVal peso As Integer)
        Me._nombre = nombre
        Me._edad = edad
        Me._sexo = sexo
        Me._altura = altura
        Me._peso = peso
    End Sub

    Public Function CalcularIMC()
        Const PesoBajo As Integer = 0
        Const Pesoideal As Integer = -1
        Const Pesoamejorar As Integer = 1
        Dim resultado As Integer = 0

        If ((Me._peso / (Me._altura ^ 2 / 100)) >= 20 And (Me.Peso / (Me._altura ^ 2 / 100)) <= 25) Then
            Console.WriteLine("Estas en tu peso ideal, muy bien sigue asi")
            Return Pesoideal
        ElseIf ((Me._peso / (Me._altura ^ 2 / 100)) < 20) Then
            Console.WriteLine("Estas en un peso bajo, procura mejorar lo")
            Return PesoBajo
        ElseIf ((Me.Peso / (Me._altura ^ 2 / 100)) > 25) Then
            Console.WriteLine("Estas en un peso Alto, debes reducir lo")
            Return Pesoamejorar
        End If
    End Function

    Public Function esMayorDeEdad()
        Dim mayoredad As Boolean = False

        If (Me._edad >= 18) Then
            mayoredad = True
            Console.WriteLine("Mayor de edad")
        Else
            mayoredad = False
            Console.WriteLine("Menor de edad")
        End If

        Return mayoredad

    End Function

    Public Sub comprobarSexo(Sexo As Char)
        If (Sexo.Equals(Me._sexo)) Then
            Console.WriteLine("Sexo Correcto")
        Else
            Me._sexo = Sexo
        End If
    End Sub

    Public Overrides Function ToString() As String
        Return "Nombre: " & Me._nombre & "Edad: " & Me._edad & "Dni:" & Me._dni & "Sexo: " & Me._sexo & "Altura: " & Me._altura & "Peso:  " & Me._peso
    End Function
End Class
