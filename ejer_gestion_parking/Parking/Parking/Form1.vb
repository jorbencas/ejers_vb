﻿Imports System.Threading

Public Class Form1
    Dim Vehiculo As Vehiculo
    Dim moto As Vehiculo
    Dim Vehiculo2 As Vehiculo
    Dim moto2 As Vehiculo
    Dim Vehiculo3 As Vehiculo
    Dim moto3 As Vehiculo
    Public VehiculosPlanta1() As Vehiculo
    Public Botonesplanta1() As Button
    Public VehiculosPlanta2() As Vehiculo
    Public Botonesplanta2() As Button
    Public VehiculosPlanta3() As Vehiculo
    Public Botonesplanta3() As Button
    Public Parkingvb As Parkingvb
    Dim botonSeleccionadoCamara As Button
    Dim fotoActual As PictureBox
    Dim listaImagenes As List(Of Image) = New List(Of Image)
    Dim auto As Boolean = False
    Private trd As Thread
    Public listaVehiculosPlanta1 As List(Of Vehiculo)
    Public listaVehiculosPlanta2 As List(Of Vehiculo)
    Public listaVehiculosPlanta3 As List(Of Vehiculo)

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles gp_btn_fp.Click
        Fijar_Preciovb.ShowDialog()
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles gp_btn_sc.Click
        SacarCuentas.ShowDialog()

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles gp_btn_gg.Click
        GestionGastosvb.ShowDialog()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles gp_btn_imprimir.Click
        MsgBox("Acude a tu impresora mas cercana, a recoger el ticket")
    End Sub

    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles gp_btn_ayuda.Click
        MsgBox("En la zona del formulario, se pueden gestionar mediante el formulario, y en todo momento se puede ver el estado en la tabla del estado de cada planta")
    End Sub



    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'Me.Parkingvb = New Parkingvb("", "", "", "", 0.0, 0.0)

        'Me.InializarBotones()
        Me.iniciarArrayImagenes(Me.listaImagenes)
        Me.CargarEventosBtnCamara()
        Me.botonSeleccionadoCamara = Me.BTN_8
        Me.fotoActual = Me.PictureBox1
    End Sub

    Public Sub InializarBotones()
        'Planta 1
        Me.Vehiculo = New Vehiculo("Coche", "369852-try")
        Me.moto = New Vehiculo("Moto", "753016-mhg")
        Me.VehiculosPlanta1 = New Vehiculo(9) {Me.Vehiculo, Me.Vehiculo, Me.moto, Me.Vehiculo, Me.Vehiculo, Me.Vehiculo, Nothing, Nothing, Nothing, Nothing}
        Me.Botonesplanta1 = New Button(9) {Me.gp_estado_btn1, Me.gp_estado_btn2, Me.gp_estado_btn3, Me.gp_estado_btn4, Me.gp_estado_btn5, Me.gp_estado_btn6, Me.gp_estado_btn7, Me.gp_estado_btn8, Me.gp_estado_btn9, Me.gp_estado_btn10}
        Cargarboton1(Me.VehiculosPlanta1.ToList, Me.Botonesplanta1.ToList)
        'Planta 2
        Me.Vehiculo2 = New Vehiculo("Coche", "369852-try")
        Me.moto2 = New Vehiculo("Moto", "753016-mhg")
        Me.VehiculosPlanta2 = New Vehiculo(9) {Me.Vehiculo2, Me.moto2, Me.moto2, Me.moto2, Me.Vehiculo2, Me.Vehiculo2, Me.Vehiculo2, Nothing, Nothing, Nothing}
        Me.Botonesplanta2 = New Button(9) {Me.gp_estado_btn11, Me.gp_estado_btn12, Me.gp_estado_btn13, Me.gp_estado_btn14, Me.gp_estado_btn15, Me.gp_estado_btn16, Me.gp_estado_btn17, Me.gp_estado_btn18, Me.gp_estado_btn19, Me.gp_estado_btn20}
        Cargarboton1(Me.VehiculosPlanta2.ToList, Me.Botonesplanta2.ToList)
        'Planta 3
        Me.Vehiculo3 = New Vehiculo("Coche", "369852-try")
        Me.moto3 = New Vehiculo("Moto", "753016-mhg")
        Me.VehiculosPlanta3 = New Vehiculo(9) {Me.Vehiculo3, Me.Vehiculo3, Me.moto3, Me.Vehiculo3, Me.Vehiculo3, Me.Vehiculo3, Me.moto3, Me.moto3, Nothing, Nothing}
        Me.Botonesplanta3 = New Button(9) {Me.gp_estado_btn21, Me.gp_estado_btn22, Me.gp_estado_btn23, Me.gp_estado_btn24, Me.gp_estado_btn25, Me.gp_estado_btn26, Me.gp_estado_btn27, Me.gp_estado_btn28, Me.gp_estado_btn29, Me.gp_estado_btn30}
        Cargarboton1(Me.VehiculosPlanta3.ToList, Me.Botonesplanta3.ToList)
    End Sub

    Public Sub Cargarboton1(ByRef listaVehiculos As List(Of Vehiculo), ByRef listaBotones As List(Of Button))
        Dim img As Image
        Dim cont As Integer = 6
        Dim cont_pieza As Integer = 7
        For index As Integer = 0 To listaBotones.Capacity - 1
            If (listaVehiculos(index) Is Nothing) Then
                If (cont_pieza < 10) Then
                    listaBotones(cont_pieza).BackColor = Color.GreenYellow
                    'Img = Image.FromFile(Application.StartupPath() & "\Dark\moto.png")
                    ' listaBotones(cont_pieza).Image = img
                    listaBotones(cont_pieza).Text = "M" & cont
                    listaBotones(cont_pieza).ForeColor = Color.Black
                    cont_pieza = cont_pieza + 1
                ElseIf (cont < 7) Then
                    listaBotones(cont).BackColor = Color.GreenYellow
                    'img = Image.FromFile(Application.StartupPath() & "\Dark\car.png")
                    listaBotones(cont).Text = "C" & cont
                    listaBotones(cont).ForeColor = Color.Black
                    'listaBotones(cont).Image = img
                    cont = cont - 1
                End If
            Else
                If (listaVehiculos(index).getTipo.Equals("Moto")) Then
                    listaBotones(cont_pieza).BackColor = Color.Red
                    'img = Image.FromFile(Application.StartupPath() & "\White\moto.png")
                    'listaBotones(cont_pieza).Image = img
                    listaBotones(cont_pieza).ForeColor = Color.Black
                    listaBotones(cont_pieza).Text = "M"
                    cont_pieza = cont_pieza + 1
                Else
                    listaBotones(index).BackColor = Color.Red
                    img = Image.FromFile(Application.StartupPath() & "\White\car.png")
                    listaBotones(index).Text = "C"
                    listaBotones(index).ForeColor = Color.Black
                    'listaBotones(index).Image = img
                End If
            End If
        Next
    End Sub



    Public Function darDeAltaVehiculosLista()
        Dim newfile As String = "bbdd.txt"
        Dim newPath As String = System.IO.Path.Combine(Application.StartupPath(), newfile)
        If System.IO.File.Exists(newPath) = True Then
            Me.listaVehiculosPlanta1 = reader(newPath, "1")
            Me.listaVehiculosPlanta2 = reader(newPath, "2")
            Me.listaVehiculosPlanta3 = reader(newPath, "3")

        Else
            Dim coche As Vehiculo = New Vehiculo("coche", "8814HJK")
            Dim coche2 As Vehiculo = New Vehiculo("coche", "123456HJK")
            Dim moto1 As Vehiculo = New Vehiculo("moto", "453463DAS")
            Me.listaVehiculosPlanta1 = New List(Of Vehiculo)({coche, coche2, moto1})
            Me.writer(newPath, Me.listaVehiculosPlanta1, 1)

            Dim coch3 As Vehiculo = New Vehiculo("coche", "123124HA")
            Dim moto2 As Vehiculo = New Vehiculo("moto", "44556HJK")
            Dim moto3 As Vehiculo = New Vehiculo("moto", "453456HAQ")
            Me.listaVehiculosPlanta2 = New List(Of Vehiculo)({coch3, moto2, moto3})
            Me.writer(newPath, Me.listaVehiculosPlanta2, 2)

            Dim coch4 As Vehiculo = New Vehiculo("coche", "756344VCF")
            Dim coche5 As Vehiculo = New Vehiculo("coche", "4565465F")
            Dim coche6 As Vehiculo = New Vehiculo("coche", "14881488HFG")
            Me.listaVehiculosPlanta3 = New List(Of Vehiculo)({coch4, coche5, coche6})
            Me.writer(newPath, Me.listaVehiculosPlanta3, 3)
        End If

    End Function



    Public Sub iniciarArrayImagenes(ByRef lista As List(Of Image))
        lista.Add(Image.FromFile(Application.StartupPath() & "\Captura.jpg"))
        lista.Add(Image.FromFile(Application.StartupPath() & "\Captura2.jpg"))
        lista.Add(Image.FromFile(Application.StartupPath() & "\Captura3.jpg"))
        lista.Add(Image.FromFile(Application.StartupPath() & "\Captura4.jpg"))
        lista.Add(Image.FromFile(Application.StartupPath() & "\Captura5.jpg"))
        lista.Add(Image.FromFile(Application.StartupPath() & "\Captura6.jpg"))
        lista.Add(Image.FromFile(Application.StartupPath() & "\Captura7.jpg"))
        lista.Add(Image.FromFile(Application.StartupPath() & "\Captura8.jpg"))
    End Sub


    Function reader(ByRef newPath As String, ByVal numPlanta As String)
        Dim list As New List(Of Vehiculo)

        Using sr As New System.IO.StreamReader(newPath)
            Dim strAux As String = "Dembow"
            While sr.Peek <> -1
                strAux = sr.ReadLine()
                Dim listaItems As String() = strAux.Split(";")

                If numPlanta = listaItems(0).ToString Then
                    Dim vehiculo As Vehiculo = New Vehiculo(listaItems(1), listaItems(2), listaItems(3), listaItems(4), Convert.ToInt32(listaItems(5)))

                    list.Add(vehiculo)

                End If

            End While
            sr.Close()
        End Using

        Return list
    End Function

    Sub writer(ByRef newPath As String, ByVal list As List(Of Vehiculo), ByVal planta As Integer)

        Using sw As New System.IO.StreamWriter(newPath, True)
            For Each item As Vehiculo In list
                sw.WriteLine(planta & ";" & item.toCSV)
            Next
            sw.Flush() ''yeap I'm the sort of person that flushes it then closes it
            sw.Close()


        End Using
    End Sub


    Private Sub CargarEventosBtnCamara()
        AddHandler Me.Automatico.Click, AddressOf Me.Automatizar
        AddHandler Me.BTN_1.Click, AddressOf Me.CambiarCamara
        AddHandler Me.BTN_2.Click, AddressOf Me.CambiarCamara
        AddHandler Me.BTN_3.Click, AddressOf Me.CambiarCamara
        AddHandler Me.BTN_4.Click, AddressOf Me.CambiarCamara
        AddHandler Me.BTN_5.Click, AddressOf Me.CambiarCamara
        AddHandler Me.BTN_6.Click, AddressOf Me.CambiarCamara
        AddHandler Me.BTN_7.Click, AddressOf Me.CambiarCamara
        AddHandler Me.BTN_8.Click, AddressOf Me.CambiarCamara
    End Sub

    Public Sub CambiarCamara(sender As Object, e As EventArgs)
        Dim automatico As Boolean = False
        Dim btn As Button = sender

        btn.BackColor = Color.DimGray
        btn.ForeColor = Color.White
        Me.botonSeleccionadoCamara.BackColor = Color.White
        Me.botonSeleccionadoCamara.ForeColor = Color.Black
        Me.botonSeleccionadoCamara = btn

        Select Case Me.botonSeleccionadoCamara.Text.Trim
            Case "1"
                Me.fotoActual.Image = Me.listaImagenes(0)
            Case "2"
                Me.fotoActual.Image = Me.listaImagenes(1)
            Case "3"
                Me.fotoActual.Image = Me.listaImagenes(2)
            Case "4"
                Me.fotoActual.Image = Me.listaImagenes(3)
            Case "5"
                Me.fotoActual.Image = Me.listaImagenes(4)
            Case "6"
                Me.fotoActual.Image = Me.listaImagenes(5)
            Case "7"
                Me.fotoActual.Image = Me.listaImagenes(6)
            Case "8"
                Me.fotoActual.Image = Me.listaImagenes(7)
        End Select
    End Sub

    Private Sub Automatizar(sender As Object, e As EventArgs)
        Dim btn As Button = sender
        Dim indice As Integer = 1
        MsgBox("Modo" & Me.auto)
        If (Me.auto Like False) Then
            Me.auto = True
            Me.PictureBox10.Image = Image.FromFile(Application.StartupPath() & "\Pause.png")
            btn.Text = "Automatico"

            Me.trd = New Thread(AddressOf cambiarImagen)
            trd.IsBackground = True
            trd.Start()

        Else
            Me.auto = False
            Me.PictureBox10.Image = Image.FromFile(Application.StartupPath() & "\Play.png")
            btn.Text = "Manual"

        End If
    End Sub

    Private Function cambiarImagen()
        While Me.auto <> False
            Dim imagenRandom As Image = Me.listaImagenes(CInt(Math.Ceiling(Rnd() * 6)) + 1)
            Me.fotoActual.Image = imagenRandom
            Thread.Sleep(1000)
        End While
    End Function

    Private Sub gp_estado_btn1_Click(sender As Object, e As EventArgs) Handles gp_estado_btn1.Click

    End Sub

    Private Sub GroupBox2_Enter(sender As Object, e As EventArgs) Handles GroupBox2.Enter

    End Sub

    Private Sub gp_combobox_SelectedIndexChanged(sender As Object, e As EventArgs) Handles gp_combobox.SelectedIndexChanged

    End Sub

    Private Sub Button1_Click_1(sender As Object, e As EventArgs) Handles Button1.Click

    End Sub

    Private Sub BTN_3_Click(sender As Object, e As EventArgs) Handles BTN_3.Click

    End Sub
End Class
