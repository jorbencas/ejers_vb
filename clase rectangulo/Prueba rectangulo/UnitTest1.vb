﻿Imports System.Text
Imports clase_rectangulo ' HAY QUE IMPORTAR LAS CLASES que queremos probar
Imports Microsoft.VisualStudio.TestTools.UnitTesting

<TestClass()> Public Class UnitTest1

    <TestMethod()> Public Sub EsCuadrado()

        'Declaramos las variables
        Dim Rec As Rectangulo = New Rectangulo(2, 2)
        Dim Si As Boolean = True
        Dim Escuadr As Boolean = True

        'Realizamos las operaciones a validar
        Escuadr = Rec.EsCuadrado()

        'Notificamos el resultado
        Assert.AreEqual(Si, Escuadr)


    End Sub

End Class