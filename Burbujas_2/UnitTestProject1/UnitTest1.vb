﻿Imports System.Text
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports Burbujas_2

<TestClass()> Public Class UnitTest1


    Dim lista As List(Of Integer) = New List(Of Integer) From {2, 3, 4, 53, 1}
    Dim vector As BurbujasClass = New BurbujasClass(lista)

    <TestMethod()> Public Sub TestMethod1()
    End Sub

    <TestMethod()> Public Sub ComprobarForPrueba()
        Dim aux As List(Of Integer) = lista
        aux.Sort()
        Assert.AreEqual(vector.burbujaFor(), aux)
    End Sub
    <TestMethod()> Public Sub ComprobarWhilePrueba()
        Dim aux As List(Of Integer) = lista
        aux.Sort()
        Assert.AreEqual(vector.burbujaFor(), aux)
    End Sub

    <TestMethod()> Public Sub ComprobarDoWhilePrueba()
        Dim aux As List(Of Integer) = lista
        aux.Sort()
        Assert.AreEqual(vector.burbujaDoWhile(), aux)
    End Sub

End Class