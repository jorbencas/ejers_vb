﻿Module Module1
    Sub Main()
        Dim contador As Integer = 0
        Dim numeros(4) As Integer

        For contador = 0 To 4
            Console.WriteLine("Introduce un número")
            numeros(contador) = Convert.ToInt32(Console.ReadLine())
        Next
        mostrar(numeros)
        suma(numeros)
        media(numeros)
        major25(numeros)
        paresimpares(numeros)
        restructurararray(numeros)
        Console.Read()
    End Sub

    Public Sub mostrar(numeros() As Integer)
        For Each number In numeros
            Console.WriteLine("El numeros es " & number)
        Next
    End Sub

    Public Sub suma(numbers() As Integer)
        Dim number As Integer = 0
        Dim total As Integer
        For Each number In numbers
            total += number
        Next
        Console.WriteLine("El total es " & total)
        resta3primerosnumeros(numbers, total)
        'Console.ReadLine()
    End Sub

    Public Sub resta3primerosnumeros(numbers() As Integer, numero As Integer)
        Dim num As Integer = 0
        Dim total As Integer = 0
        Dim newtotal As Integer = 0

        For contador As Integer = 0 To 2
            Console.WriteLine("Contador:" & contador)
            Console.WriteLine("El numero a sumar:" & numbers(contador))
            newtotal += Int(numbers(contador))
        Next
        Console.WriteLine("Suma de los 2 primeros numeros del array:" & newtotal)
        total = (numero - newtotal)

        Console.WriteLine("El total de la resta es " & total)
    End Sub

    Public Sub media(numbers() As Integer)
        Dim number As Integer = 0
        Dim total As Integer
        For Each number In numbers
            total += number
        Next
        total = (total / Convert.ToInt32(numbers.Length))
        Console.WriteLine("La media aritmetica es " & total)
    End Sub

    Public Sub major25(numbers() As Integer)
        For Each number In numbers
            If number > 25 Then
                Console.WriteLine(number & " Es major de 25")
            Else
                Console.WriteLine(number & " Es menor de 25")
            End If
        Next
    End Sub
    Public Sub paresimpares(numbers() As Integer)

        For Each number In numbers
            If number = Int(number / 2) * 2 Then
                Console.WriteLine(number & "es par")
            Else
                Console.WriteLine(number & "es impar")
            End If
        Next
    End Sub

    Public Sub restructurararray(numbers() As Integer)
        ReDim Preserve numbers(3)
        Console.WriteLine("

                    Ultimo Ejercicio 
                            
                            ")
        mostrar(numbers)
        suma(numbers)
        media(numbers)
        major25(numbers)
        paresimpares(numbers)
        Console.Read()
    End Sub
End Module
