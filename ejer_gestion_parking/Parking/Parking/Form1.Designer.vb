﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.gp_input_matricula = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.gp_combobox = New System.Windows.Forms.ComboBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.gp_datepicker_salida_fin = New System.Windows.Forms.DateTimePicker()
        Me.gp_datepicker_salida_inicio = New System.Windows.Forms.DateTimePicker()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.gp_btn_imprimir = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.gp_datepicker_entrada_inicio = New System.Windows.Forms.DateTimePicker()
        Me.gp_datepicker_entrada_fin = New System.Windows.Forms.DateTimePicker()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.gp_btn_gg = New System.Windows.Forms.Button()
        Me.gp_btn_fp = New System.Windows.Forms.Button()
        Me.gp_btn_sc = New System.Windows.Forms.Button()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.GroupBox7 = New System.Windows.Forms.GroupBox()
        Me.gp_estado_btn10 = New System.Windows.Forms.Button()
        Me.gp_estado_btn5 = New System.Windows.Forms.Button()
        Me.gp_estado_btn9 = New System.Windows.Forms.Button()
        Me.gp_estado_btn8 = New System.Windows.Forms.Button()
        Me.gp_estado_btn7 = New System.Windows.Forms.Button()
        Me.gp_estado_btn6 = New System.Windows.Forms.Button()
        Me.gp_estado_btn4 = New System.Windows.Forms.Button()
        Me.gp_estado_btn3 = New System.Windows.Forms.Button()
        Me.gp_estado_btn2 = New System.Windows.Forms.Button()
        Me.gp_estado_btn1 = New System.Windows.Forms.Button()
        Me.gp_btn_ayuda = New System.Windows.Forms.Button()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.gp_estado_btn15 = New System.Windows.Forms.Button()
        Me.gp_estado_btn16 = New System.Windows.Forms.Button()
        Me.gp_estado_btn13 = New System.Windows.Forms.Button()
        Me.gp_estado_btn11 = New System.Windows.Forms.Button()
        Me.gp_estado_btn25 = New System.Windows.Forms.Button()
        Me.gp_estado_btn28 = New System.Windows.Forms.Button()
        Me.gp_estado_btn26 = New System.Windows.Forms.Button()
        Me.gp_estado_btn23 = New System.Windows.Forms.Button()
        Me.gp_estado_btn21 = New System.Windows.Forms.Button()
        Me.gp_estado_btn12 = New System.Windows.Forms.Button()
        Me.gp_estado_btn29 = New System.Windows.Forms.Button()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.gp_estado_btn18 = New System.Windows.Forms.Button()
        Me.gp_estado_btn20 = New System.Windows.Forms.Button()
        Me.gp_estado_btn19 = New System.Windows.Forms.Button()
        Me.gp_estado_btn17 = New System.Windows.Forms.Button()
        Me.gp_estado_btn14 = New System.Windows.Forms.Button()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.gp_estado_btn30 = New System.Windows.Forms.Button()
        Me.gp_estado_btn27 = New System.Windows.Forms.Button()
        Me.gp_estado_btn24 = New System.Windows.Forms.Button()
        Me.gp_estado_btn22 = New System.Windows.Forms.Button()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.BTN_1 = New System.Windows.Forms.Button()
        Me.BTN_2 = New System.Windows.Forms.Button()
        Me.BTN_3 = New System.Windows.Forms.Button()
        Me.BTN_4 = New System.Windows.Forms.Button()
        Me.BTN_5 = New System.Windows.Forms.Button()
        Me.BTN_6 = New System.Windows.Forms.Button()
        Me.BTN_7 = New System.Windows.Forms.Button()
        Me.BTN_8 = New System.Windows.Forms.Button()
        Me.Automatico = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox7.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gp_input_matricula
        '
        Me.gp_input_matricula.Location = New System.Drawing.Point(15, 102)
        Me.gp_input_matricula.Name = "gp_input_matricula"
        Me.gp_input_matricula.Size = New System.Drawing.Size(175, 20)
        Me.gp_input_matricula.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(12, 61)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(62, 16)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Matricula"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(377, 9)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(185, 25)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Gestor de Parking"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(28, 192)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(107, 16)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Hora de Entrada"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(259, 58)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(110, 16)
        Me.Label5.TabIndex = 7
        Me.Label5.Text = "Tipo de Vehiculo"
        '
        'gp_combobox
        '
        Me.gp_combobox.FormattingEnabled = True
        Me.gp_combobox.Items.AddRange(New Object() {"Coche", "Moto"})
        Me.gp_combobox.Location = New System.Drawing.Point(253, 101)
        Me.gp_combobox.Name = "gp_combobox"
        Me.gp_combobox.Size = New System.Drawing.Size(175, 21)
        Me.gp_combobox.TabIndex = 8
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(51, 25)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(120, 16)
        Me.Label6.TabIndex = 9
        Me.Label6.Text = "Precio de estancia"
        '
        'TextBox3
        '
        Me.TextBox3.Location = New System.Drawing.Point(54, 54)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(221, 20)
        Me.TextBox3.TabIndex = 10
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.gp_datepicker_salida_fin)
        Me.GroupBox1.Controls.Add(Me.gp_datepicker_salida_inicio)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.TextBox3)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Location = New System.Drawing.Point(532, 77)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(338, 338)
        Me.GroupBox1.TabIndex = 11
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Salida"
        '
        'gp_datepicker_salida_fin
        '
        Me.gp_datepicker_salida_fin.CustomFormat = "dd:MM:yyyy"
        Me.gp_datepicker_salida_fin.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.gp_datepicker_salida_fin.Location = New System.Drawing.Point(54, 234)
        Me.gp_datepicker_salida_fin.Name = "gp_datepicker_salida_fin"
        Me.gp_datepicker_salida_fin.Size = New System.Drawing.Size(221, 20)
        Me.gp_datepicker_salida_fin.TabIndex = 17
        '
        'gp_datepicker_salida_inicio
        '
        Me.gp_datepicker_salida_inicio.CustomFormat = "HH:mm"
        Me.gp_datepicker_salida_inicio.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.gp_datepicker_salida_inicio.Location = New System.Drawing.Point(54, 139)
        Me.gp_datepicker_salida_inicio.Name = "gp_datepicker_salida_inicio"
        Me.gp_datepicker_salida_inicio.ShowUpDown = True
        Me.gp_datepicker_salida_inicio.Size = New System.Drawing.Size(221, 20)
        Me.gp_datepicker_salida_inicio.TabIndex = 16
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(51, 192)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(107, 16)
        Me.Label8.TabIndex = 13
        Me.Label8.Text = "Fecha de Salida"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(51, 102)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(99, 16)
        Me.Label7.TabIndex = 11
        Me.Label7.Text = "Hora de Salida"
        '
        'gp_btn_imprimir
        '
        Me.gp_btn_imprimir.Image = CType(resources.GetObject("gp_btn_imprimir.Image"), System.Drawing.Image)
        Me.gp_btn_imprimir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.gp_btn_imprimir.Location = New System.Drawing.Point(676, 424)
        Me.gp_btn_imprimir.Name = "gp_btn_imprimir"
        Me.gp_btn_imprimir.Size = New System.Drawing.Size(85, 37)
        Me.gp_btn_imprimir.TabIndex = 15
        Me.gp_btn_imprimir.Text = "Imprimir"
        Me.gp_btn_imprimir.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.gp_btn_imprimir.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.gp_datepicker_entrada_inicio)
        Me.GroupBox2.Controls.Add(Me.gp_datepicker_entrada_fin)
        Me.GroupBox2.Controls.Add(Me.Label9)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.gp_combobox)
        Me.GroupBox2.Controls.Add(Me.gp_input_matricula)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 77)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(497, 338)
        Me.GroupBox2.TabIndex = 12
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Entrada"
        '
        'gp_datepicker_entrada_inicio
        '
        Me.gp_datepicker_entrada_inicio.CustomFormat = "HH:mm"
        Me.gp_datepicker_entrada_inicio.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.gp_datepicker_entrada_inicio.Location = New System.Drawing.Point(31, 233)
        Me.gp_datepicker_entrada_inicio.Name = "gp_datepicker_entrada_inicio"
        Me.gp_datepicker_entrada_inicio.ShowUpDown = True
        Me.gp_datepicker_entrada_inicio.Size = New System.Drawing.Size(171, 20)
        Me.gp_datepicker_entrada_inicio.TabIndex = 11
        '
        'gp_datepicker_entrada_fin
        '
        Me.gp_datepicker_entrada_fin.CustomFormat = "dd:MM:yyyy"
        Me.gp_datepicker_entrada_fin.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.gp_datepicker_entrada_fin.Location = New System.Drawing.Point(265, 233)
        Me.gp_datepicker_entrada_fin.Name = "gp_datepicker_entrada_fin"
        Me.gp_datepicker_entrada_fin.Size = New System.Drawing.Size(175, 20)
        Me.gp_datepicker_entrada_fin.TabIndex = 10
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(259, 189)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(115, 16)
        Me.Label9.TabIndex = 9
        Me.Label9.Text = "Fecha de Entrada"
        '
        'Button1
        '
        Me.Button1.Image = CType(resources.GetObject("Button1.Image"), System.Drawing.Image)
        Me.Button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button1.Location = New System.Drawing.Point(209, 424)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(88, 37)
        Me.Button1.TabIndex = 12
        Me.Button1.Text = "Registrar"
        Me.Button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button1.UseVisualStyleBackColor = True
        '
        'gp_btn_gg
        '
        Me.gp_btn_gg.Image = CType(resources.GetObject("gp_btn_gg.Image"), System.Drawing.Image)
        Me.gp_btn_gg.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.gp_btn_gg.Location = New System.Drawing.Point(1096, 538)
        Me.gp_btn_gg.Name = "gp_btn_gg"
        Me.gp_btn_gg.Size = New System.Drawing.Size(137, 43)
        Me.gp_btn_gg.TabIndex = 13
        Me.gp_btn_gg.Text = "Gestión de gastos"
        Me.gp_btn_gg.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.gp_btn_gg.UseVisualStyleBackColor = True
        '
        'gp_btn_fp
        '
        Me.gp_btn_fp.Image = CType(resources.GetObject("gp_btn_fp.Image"), System.Drawing.Image)
        Me.gp_btn_fp.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.gp_btn_fp.Location = New System.Drawing.Point(1098, 650)
        Me.gp_btn_fp.Name = "gp_btn_fp"
        Me.gp_btn_fp.Size = New System.Drawing.Size(135, 45)
        Me.gp_btn_fp.TabIndex = 14
        Me.gp_btn_fp.Text = "Fijar Precios"
        Me.gp_btn_fp.UseVisualStyleBackColor = True
        '
        'gp_btn_sc
        '
        Me.gp_btn_sc.Image = CType(resources.GetObject("gp_btn_sc.Image"), System.Drawing.Image)
        Me.gp_btn_sc.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.gp_btn_sc.Location = New System.Drawing.Point(1096, 598)
        Me.gp_btn_sc.Name = "gp_btn_sc"
        Me.gp_btn_sc.Size = New System.Drawing.Size(137, 36)
        Me.gp_btn_sc.TabIndex = 15
        Me.gp_btn_sc.Text = "Sacar Cuota"
        Me.gp_btn_sc.UseVisualStyleBackColor = True
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(1047, 10)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(167, 24)
        Me.Label10.TabIndex = 20
        Me.Label10.Text = "Estado del Parking"
        '
        'GroupBox7
        '
        Me.GroupBox7.Controls.Add(Me.gp_estado_btn10)
        Me.GroupBox7.Controls.Add(Me.gp_estado_btn5)
        Me.GroupBox7.Controls.Add(Me.gp_estado_btn9)
        Me.GroupBox7.Controls.Add(Me.gp_estado_btn8)
        Me.GroupBox7.Controls.Add(Me.gp_estado_btn7)
        Me.GroupBox7.Controls.Add(Me.gp_estado_btn6)
        Me.GroupBox7.Controls.Add(Me.gp_estado_btn4)
        Me.GroupBox7.Controls.Add(Me.gp_estado_btn3)
        Me.GroupBox7.Controls.Add(Me.gp_estado_btn2)
        Me.GroupBox7.Controls.Add(Me.gp_estado_btn1)
        Me.GroupBox7.Location = New System.Drawing.Point(915, 58)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(443, 137)
        Me.GroupBox7.TabIndex = 24
        Me.GroupBox7.TabStop = False
        Me.GroupBox7.Text = "Planta 1"
        '
        'gp_estado_btn10
        '
        Me.gp_estado_btn10.BackColor = System.Drawing.Color.Red
        Me.gp_estado_btn10.Cursor = System.Windows.Forms.Cursors.Hand
        Me.gp_estado_btn10.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gp_estado_btn10.ForeColor = System.Drawing.Color.White
        Me.gp_estado_btn10.Location = New System.Drawing.Point(364, 81)
        Me.gp_estado_btn10.Name = "gp_estado_btn10"
        Me.gp_estado_btn10.Size = New System.Drawing.Size(48, 48)
        Me.gp_estado_btn10.TabIndex = 84
        Me.gp_estado_btn10.UseVisualStyleBackColor = False
        '
        'gp_estado_btn5
        '
        Me.gp_estado_btn5.BackColor = System.Drawing.Color.GreenYellow
        Me.gp_estado_btn5.Cursor = System.Windows.Forms.Cursors.Hand
        Me.gp_estado_btn5.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gp_estado_btn5.ForeColor = System.Drawing.Color.White
        Me.gp_estado_btn5.Location = New System.Drawing.Point(364, 18)
        Me.gp_estado_btn5.Name = "gp_estado_btn5"
        Me.gp_estado_btn5.Size = New System.Drawing.Size(48, 48)
        Me.gp_estado_btn5.TabIndex = 83
        Me.ToolTip1.SetToolTip(Me.gp_estado_btn5, "Moto")
        Me.gp_estado_btn5.UseVisualStyleBackColor = False
        '
        'gp_estado_btn9
        '
        Me.gp_estado_btn9.BackColor = System.Drawing.Color.Red
        Me.gp_estado_btn9.Cursor = System.Windows.Forms.Cursors.Hand
        Me.gp_estado_btn9.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gp_estado_btn9.ForeColor = System.Drawing.Color.White
        Me.gp_estado_btn9.Location = New System.Drawing.Point(284, 81)
        Me.gp_estado_btn9.Name = "gp_estado_btn9"
        Me.gp_estado_btn9.Size = New System.Drawing.Size(48, 48)
        Me.gp_estado_btn9.TabIndex = 82
        Me.gp_estado_btn9.UseVisualStyleBackColor = False
        '
        'gp_estado_btn8
        '
        Me.gp_estado_btn8.BackColor = System.Drawing.Color.GreenYellow
        Me.gp_estado_btn8.Cursor = System.Windows.Forms.Cursors.Hand
        Me.gp_estado_btn8.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gp_estado_btn8.ForeColor = System.Drawing.Color.White
        Me.gp_estado_btn8.Location = New System.Drawing.Point(200, 82)
        Me.gp_estado_btn8.Name = "gp_estado_btn8"
        Me.gp_estado_btn8.Size = New System.Drawing.Size(48, 48)
        Me.gp_estado_btn8.TabIndex = 81
        Me.ToolTip1.SetToolTip(Me.gp_estado_btn8, "Moto")
        Me.gp_estado_btn8.UseVisualStyleBackColor = False
        '
        'gp_estado_btn7
        '
        Me.gp_estado_btn7.BackColor = System.Drawing.Color.Red
        Me.gp_estado_btn7.Cursor = System.Windows.Forms.Cursors.Hand
        Me.gp_estado_btn7.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gp_estado_btn7.ForeColor = System.Drawing.Color.White
        Me.gp_estado_btn7.Location = New System.Drawing.Point(115, 81)
        Me.gp_estado_btn7.Name = "gp_estado_btn7"
        Me.gp_estado_btn7.Size = New System.Drawing.Size(48, 48)
        Me.gp_estado_btn7.TabIndex = 80
        Me.gp_estado_btn7.UseVisualStyleBackColor = False
        '
        'gp_estado_btn6
        '
        Me.gp_estado_btn6.BackColor = System.Drawing.Color.GreenYellow
        Me.gp_estado_btn6.Cursor = System.Windows.Forms.Cursors.Hand
        Me.gp_estado_btn6.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gp_estado_btn6.ForeColor = System.Drawing.Color.White
        Me.gp_estado_btn6.Location = New System.Drawing.Point(38, 80)
        Me.gp_estado_btn6.Name = "gp_estado_btn6"
        Me.gp_estado_btn6.Size = New System.Drawing.Size(48, 48)
        Me.gp_estado_btn6.TabIndex = 79
        Me.ToolTip1.SetToolTip(Me.gp_estado_btn6, "Moto")
        Me.gp_estado_btn6.UseVisualStyleBackColor = False
        '
        'gp_estado_btn4
        '
        Me.gp_estado_btn4.BackColor = System.Drawing.Color.Red
        Me.gp_estado_btn4.Cursor = System.Windows.Forms.Cursors.Hand
        Me.gp_estado_btn4.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gp_estado_btn4.ForeColor = System.Drawing.Color.White
        Me.gp_estado_btn4.Location = New System.Drawing.Point(284, 19)
        Me.gp_estado_btn4.Name = "gp_estado_btn4"
        Me.gp_estado_btn4.Size = New System.Drawing.Size(48, 48)
        Me.gp_estado_btn4.TabIndex = 78
        Me.gp_estado_btn4.UseVisualStyleBackColor = False
        '
        'gp_estado_btn3
        '
        Me.gp_estado_btn3.BackColor = System.Drawing.Color.GreenYellow
        Me.gp_estado_btn3.Cursor = System.Windows.Forms.Cursors.Hand
        Me.gp_estado_btn3.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gp_estado_btn3.ForeColor = System.Drawing.Color.White
        Me.gp_estado_btn3.Location = New System.Drawing.Point(200, 19)
        Me.gp_estado_btn3.Name = "gp_estado_btn3"
        Me.gp_estado_btn3.Size = New System.Drawing.Size(48, 48)
        Me.gp_estado_btn3.TabIndex = 77
        Me.ToolTip1.SetToolTip(Me.gp_estado_btn3, "Moto")
        Me.gp_estado_btn3.UseVisualStyleBackColor = False
        '
        'gp_estado_btn2
        '
        Me.gp_estado_btn2.BackColor = System.Drawing.Color.Red
        Me.gp_estado_btn2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.gp_estado_btn2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gp_estado_btn2.ForeColor = System.Drawing.Color.White
        Me.gp_estado_btn2.Location = New System.Drawing.Point(115, 23)
        Me.gp_estado_btn2.Name = "gp_estado_btn2"
        Me.gp_estado_btn2.Size = New System.Drawing.Size(48, 48)
        Me.gp_estado_btn2.TabIndex = 68
        Me.gp_estado_btn2.UseVisualStyleBackColor = False
        '
        'gp_estado_btn1
        '
        Me.gp_estado_btn1.BackColor = System.Drawing.Color.GreenYellow
        Me.gp_estado_btn1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.gp_estado_btn1.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gp_estado_btn1.ForeColor = System.Drawing.Color.White
        Me.gp_estado_btn1.Location = New System.Drawing.Point(38, 22)
        Me.gp_estado_btn1.Name = "gp_estado_btn1"
        Me.gp_estado_btn1.Size = New System.Drawing.Size(48, 48)
        Me.gp_estado_btn1.TabIndex = 67
        Me.ToolTip1.SetToolTip(Me.gp_estado_btn1, "Moto")
        Me.gp_estado_btn1.UseVisualStyleBackColor = False
        '
        'gp_btn_ayuda
        '
        Me.gp_btn_ayuda.BackColor = System.Drawing.Color.Gainsboro
        Me.gp_btn_ayuda.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gp_btn_ayuda.ForeColor = System.Drawing.Color.Black
        Me.gp_btn_ayuda.Image = CType(resources.GetObject("gp_btn_ayuda.Image"), System.Drawing.Image)
        Me.gp_btn_ayuda.Location = New System.Drawing.Point(12, 423)
        Me.gp_btn_ayuda.Name = "gp_btn_ayuda"
        Me.gp_btn_ayuda.Size = New System.Drawing.Size(32, 34)
        Me.gp_btn_ayuda.TabIndex = 13
        Me.gp_btn_ayuda.UseVisualStyleBackColor = False
        '
        'ToolTip1
        '
        Me.ToolTip1.OwnerDraw = True
        Me.ToolTip1.ShowAlways = True
        Me.ToolTip1.StripAmpersands = True
        Me.ToolTip1.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info
        Me.ToolTip1.ToolTipTitle = "Información del tipo de vehiculo"
        '
        'gp_estado_btn15
        '
        Me.gp_estado_btn15.BackColor = System.Drawing.Color.GreenYellow
        Me.gp_estado_btn15.Cursor = System.Windows.Forms.Cursors.Hand
        Me.gp_estado_btn15.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gp_estado_btn15.ForeColor = System.Drawing.Color.White
        Me.gp_estado_btn15.Location = New System.Drawing.Point(364, 18)
        Me.gp_estado_btn15.Name = "gp_estado_btn15"
        Me.gp_estado_btn15.Size = New System.Drawing.Size(48, 48)
        Me.gp_estado_btn15.TabIndex = 83
        Me.ToolTip1.SetToolTip(Me.gp_estado_btn15, "Moto")
        Me.gp_estado_btn15.UseVisualStyleBackColor = False
        '
        'gp_estado_btn16
        '
        Me.gp_estado_btn16.BackColor = System.Drawing.Color.GreenYellow
        Me.gp_estado_btn16.Cursor = System.Windows.Forms.Cursors.Hand
        Me.gp_estado_btn16.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gp_estado_btn16.ForeColor = System.Drawing.Color.White
        Me.gp_estado_btn16.Location = New System.Drawing.Point(38, 80)
        Me.gp_estado_btn16.Name = "gp_estado_btn16"
        Me.gp_estado_btn16.Size = New System.Drawing.Size(48, 48)
        Me.gp_estado_btn16.TabIndex = 79
        Me.ToolTip1.SetToolTip(Me.gp_estado_btn16, "Moto")
        Me.gp_estado_btn16.UseVisualStyleBackColor = False
        '
        'gp_estado_btn13
        '
        Me.gp_estado_btn13.BackColor = System.Drawing.Color.GreenYellow
        Me.gp_estado_btn13.Cursor = System.Windows.Forms.Cursors.Hand
        Me.gp_estado_btn13.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gp_estado_btn13.ForeColor = System.Drawing.Color.White
        Me.gp_estado_btn13.Location = New System.Drawing.Point(200, 19)
        Me.gp_estado_btn13.Name = "gp_estado_btn13"
        Me.gp_estado_btn13.Size = New System.Drawing.Size(48, 48)
        Me.gp_estado_btn13.TabIndex = 77
        Me.ToolTip1.SetToolTip(Me.gp_estado_btn13, "Moto")
        Me.gp_estado_btn13.UseVisualStyleBackColor = False
        '
        'gp_estado_btn11
        '
        Me.gp_estado_btn11.BackColor = System.Drawing.Color.GreenYellow
        Me.gp_estado_btn11.Cursor = System.Windows.Forms.Cursors.Hand
        Me.gp_estado_btn11.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gp_estado_btn11.ForeColor = System.Drawing.Color.White
        Me.gp_estado_btn11.Location = New System.Drawing.Point(38, 22)
        Me.gp_estado_btn11.Name = "gp_estado_btn11"
        Me.gp_estado_btn11.Size = New System.Drawing.Size(48, 48)
        Me.gp_estado_btn11.TabIndex = 67
        Me.ToolTip1.SetToolTip(Me.gp_estado_btn11, "Moto")
        Me.gp_estado_btn11.UseVisualStyleBackColor = False
        '
        'gp_estado_btn25
        '
        Me.gp_estado_btn25.BackColor = System.Drawing.Color.GreenYellow
        Me.gp_estado_btn25.Cursor = System.Windows.Forms.Cursors.Hand
        Me.gp_estado_btn25.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gp_estado_btn25.ForeColor = System.Drawing.Color.White
        Me.gp_estado_btn25.Location = New System.Drawing.Point(364, 18)
        Me.gp_estado_btn25.Name = "gp_estado_btn25"
        Me.gp_estado_btn25.Size = New System.Drawing.Size(48, 48)
        Me.gp_estado_btn25.TabIndex = 83
        Me.ToolTip1.SetToolTip(Me.gp_estado_btn25, "Moto")
        Me.gp_estado_btn25.UseVisualStyleBackColor = False
        '
        'gp_estado_btn28
        '
        Me.gp_estado_btn28.BackColor = System.Drawing.Color.GreenYellow
        Me.gp_estado_btn28.Cursor = System.Windows.Forms.Cursors.Hand
        Me.gp_estado_btn28.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gp_estado_btn28.ForeColor = System.Drawing.Color.White
        Me.gp_estado_btn28.Location = New System.Drawing.Point(200, 82)
        Me.gp_estado_btn28.Name = "gp_estado_btn28"
        Me.gp_estado_btn28.Size = New System.Drawing.Size(48, 48)
        Me.gp_estado_btn28.TabIndex = 81
        Me.ToolTip1.SetToolTip(Me.gp_estado_btn28, "Moto")
        Me.gp_estado_btn28.UseVisualStyleBackColor = False
        '
        'gp_estado_btn26
        '
        Me.gp_estado_btn26.BackColor = System.Drawing.Color.GreenYellow
        Me.gp_estado_btn26.Cursor = System.Windows.Forms.Cursors.Hand
        Me.gp_estado_btn26.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gp_estado_btn26.ForeColor = System.Drawing.Color.White
        Me.gp_estado_btn26.Location = New System.Drawing.Point(38, 80)
        Me.gp_estado_btn26.Name = "gp_estado_btn26"
        Me.gp_estado_btn26.Size = New System.Drawing.Size(48, 48)
        Me.gp_estado_btn26.TabIndex = 79
        Me.ToolTip1.SetToolTip(Me.gp_estado_btn26, "Moto")
        Me.gp_estado_btn26.UseVisualStyleBackColor = False
        '
        'gp_estado_btn23
        '
        Me.gp_estado_btn23.BackColor = System.Drawing.Color.GreenYellow
        Me.gp_estado_btn23.Cursor = System.Windows.Forms.Cursors.Hand
        Me.gp_estado_btn23.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gp_estado_btn23.ForeColor = System.Drawing.Color.White
        Me.gp_estado_btn23.Location = New System.Drawing.Point(200, 19)
        Me.gp_estado_btn23.Name = "gp_estado_btn23"
        Me.gp_estado_btn23.Size = New System.Drawing.Size(48, 48)
        Me.gp_estado_btn23.TabIndex = 77
        Me.ToolTip1.SetToolTip(Me.gp_estado_btn23, "Moto")
        Me.gp_estado_btn23.UseVisualStyleBackColor = False
        '
        'gp_estado_btn21
        '
        Me.gp_estado_btn21.BackColor = System.Drawing.Color.GreenYellow
        Me.gp_estado_btn21.Cursor = System.Windows.Forms.Cursors.Hand
        Me.gp_estado_btn21.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gp_estado_btn21.ForeColor = System.Drawing.Color.White
        Me.gp_estado_btn21.Location = New System.Drawing.Point(38, 22)
        Me.gp_estado_btn21.Name = "gp_estado_btn21"
        Me.gp_estado_btn21.Size = New System.Drawing.Size(48, 48)
        Me.gp_estado_btn21.TabIndex = 67
        Me.ToolTip1.SetToolTip(Me.gp_estado_btn21, "Moto")
        Me.gp_estado_btn21.UseVisualStyleBackColor = False
        '
        'gp_estado_btn12
        '
        Me.gp_estado_btn12.BackColor = System.Drawing.Color.GreenYellow
        Me.gp_estado_btn12.Cursor = System.Windows.Forms.Cursors.Hand
        Me.gp_estado_btn12.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gp_estado_btn12.ForeColor = System.Drawing.Color.White
        Me.gp_estado_btn12.Location = New System.Drawing.Point(115, 21)
        Me.gp_estado_btn12.Name = "gp_estado_btn12"
        Me.gp_estado_btn12.Size = New System.Drawing.Size(48, 48)
        Me.gp_estado_btn12.TabIndex = 86
        Me.ToolTip1.SetToolTip(Me.gp_estado_btn12, "Moto")
        Me.gp_estado_btn12.UseVisualStyleBackColor = False
        '
        'gp_estado_btn29
        '
        Me.gp_estado_btn29.BackColor = System.Drawing.Color.GreenYellow
        Me.gp_estado_btn29.Cursor = System.Windows.Forms.Cursors.Hand
        Me.gp_estado_btn29.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gp_estado_btn29.ForeColor = System.Drawing.Color.White
        Me.gp_estado_btn29.Location = New System.Drawing.Point(284, 80)
        Me.gp_estado_btn29.Name = "gp_estado_btn29"
        Me.gp_estado_btn29.Size = New System.Drawing.Size(48, 48)
        Me.gp_estado_btn29.TabIndex = 87
        Me.ToolTip1.SetToolTip(Me.gp_estado_btn29, "Moto")
        Me.gp_estado_btn29.UseVisualStyleBackColor = False
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.gp_estado_btn12)
        Me.GroupBox3.Controls.Add(Me.gp_estado_btn18)
        Me.GroupBox3.Controls.Add(Me.gp_estado_btn20)
        Me.GroupBox3.Controls.Add(Me.gp_estado_btn15)
        Me.GroupBox3.Controls.Add(Me.gp_estado_btn19)
        Me.GroupBox3.Controls.Add(Me.gp_estado_btn17)
        Me.GroupBox3.Controls.Add(Me.gp_estado_btn16)
        Me.GroupBox3.Controls.Add(Me.gp_estado_btn14)
        Me.GroupBox3.Controls.Add(Me.gp_estado_btn13)
        Me.GroupBox3.Controls.Add(Me.gp_estado_btn11)
        Me.GroupBox3.Location = New System.Drawing.Point(915, 216)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(443, 137)
        Me.GroupBox3.TabIndex = 25
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Planta 1"
        '
        'gp_estado_btn18
        '
        Me.gp_estado_btn18.BackColor = System.Drawing.Color.Red
        Me.gp_estado_btn18.Cursor = System.Windows.Forms.Cursors.Hand
        Me.gp_estado_btn18.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gp_estado_btn18.ForeColor = System.Drawing.Color.White
        Me.gp_estado_btn18.Location = New System.Drawing.Point(200, 80)
        Me.gp_estado_btn18.Name = "gp_estado_btn18"
        Me.gp_estado_btn18.Size = New System.Drawing.Size(48, 48)
        Me.gp_estado_btn18.TabIndex = 85
        Me.gp_estado_btn18.UseVisualStyleBackColor = False
        '
        'gp_estado_btn20
        '
        Me.gp_estado_btn20.BackColor = System.Drawing.Color.Red
        Me.gp_estado_btn20.Cursor = System.Windows.Forms.Cursors.Hand
        Me.gp_estado_btn20.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gp_estado_btn20.ForeColor = System.Drawing.Color.White
        Me.gp_estado_btn20.Location = New System.Drawing.Point(364, 81)
        Me.gp_estado_btn20.Name = "gp_estado_btn20"
        Me.gp_estado_btn20.Size = New System.Drawing.Size(48, 48)
        Me.gp_estado_btn20.TabIndex = 84
        Me.gp_estado_btn20.UseVisualStyleBackColor = False
        '
        'gp_estado_btn19
        '
        Me.gp_estado_btn19.BackColor = System.Drawing.Color.Red
        Me.gp_estado_btn19.Cursor = System.Windows.Forms.Cursors.Hand
        Me.gp_estado_btn19.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gp_estado_btn19.ForeColor = System.Drawing.Color.White
        Me.gp_estado_btn19.Location = New System.Drawing.Point(284, 81)
        Me.gp_estado_btn19.Name = "gp_estado_btn19"
        Me.gp_estado_btn19.Size = New System.Drawing.Size(48, 48)
        Me.gp_estado_btn19.TabIndex = 82
        Me.gp_estado_btn19.UseVisualStyleBackColor = False
        '
        'gp_estado_btn17
        '
        Me.gp_estado_btn17.BackColor = System.Drawing.Color.Red
        Me.gp_estado_btn17.Cursor = System.Windows.Forms.Cursors.Hand
        Me.gp_estado_btn17.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gp_estado_btn17.ForeColor = System.Drawing.Color.White
        Me.gp_estado_btn17.Location = New System.Drawing.Point(115, 81)
        Me.gp_estado_btn17.Name = "gp_estado_btn17"
        Me.gp_estado_btn17.Size = New System.Drawing.Size(48, 48)
        Me.gp_estado_btn17.TabIndex = 80
        Me.gp_estado_btn17.UseVisualStyleBackColor = False
        '
        'gp_estado_btn14
        '
        Me.gp_estado_btn14.BackColor = System.Drawing.Color.Red
        Me.gp_estado_btn14.Cursor = System.Windows.Forms.Cursors.Hand
        Me.gp_estado_btn14.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gp_estado_btn14.ForeColor = System.Drawing.Color.White
        Me.gp_estado_btn14.Location = New System.Drawing.Point(284, 19)
        Me.gp_estado_btn14.Name = "gp_estado_btn14"
        Me.gp_estado_btn14.Size = New System.Drawing.Size(48, 48)
        Me.gp_estado_btn14.TabIndex = 78
        Me.gp_estado_btn14.UseVisualStyleBackColor = False
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.gp_estado_btn29)
        Me.GroupBox4.Controls.Add(Me.gp_estado_btn30)
        Me.GroupBox4.Controls.Add(Me.gp_estado_btn25)
        Me.GroupBox4.Controls.Add(Me.gp_estado_btn28)
        Me.GroupBox4.Controls.Add(Me.gp_estado_btn27)
        Me.GroupBox4.Controls.Add(Me.gp_estado_btn26)
        Me.GroupBox4.Controls.Add(Me.gp_estado_btn24)
        Me.GroupBox4.Controls.Add(Me.gp_estado_btn23)
        Me.GroupBox4.Controls.Add(Me.gp_estado_btn22)
        Me.GroupBox4.Controls.Add(Me.gp_estado_btn21)
        Me.GroupBox4.Location = New System.Drawing.Point(915, 363)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(443, 137)
        Me.GroupBox4.TabIndex = 26
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Planta 1"
        '
        'gp_estado_btn30
        '
        Me.gp_estado_btn30.BackColor = System.Drawing.Color.Red
        Me.gp_estado_btn30.Cursor = System.Windows.Forms.Cursors.Hand
        Me.gp_estado_btn30.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gp_estado_btn30.ForeColor = System.Drawing.Color.White
        Me.gp_estado_btn30.Location = New System.Drawing.Point(364, 81)
        Me.gp_estado_btn30.Name = "gp_estado_btn30"
        Me.gp_estado_btn30.Size = New System.Drawing.Size(48, 48)
        Me.gp_estado_btn30.TabIndex = 84
        Me.gp_estado_btn30.UseVisualStyleBackColor = False
        '
        'gp_estado_btn27
        '
        Me.gp_estado_btn27.BackColor = System.Drawing.Color.Red
        Me.gp_estado_btn27.Cursor = System.Windows.Forms.Cursors.Hand
        Me.gp_estado_btn27.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gp_estado_btn27.ForeColor = System.Drawing.Color.White
        Me.gp_estado_btn27.Location = New System.Drawing.Point(115, 81)
        Me.gp_estado_btn27.Name = "gp_estado_btn27"
        Me.gp_estado_btn27.Size = New System.Drawing.Size(48, 48)
        Me.gp_estado_btn27.TabIndex = 80
        Me.gp_estado_btn27.UseVisualStyleBackColor = False
        '
        'gp_estado_btn24
        '
        Me.gp_estado_btn24.BackColor = System.Drawing.Color.Red
        Me.gp_estado_btn24.Cursor = System.Windows.Forms.Cursors.Hand
        Me.gp_estado_btn24.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gp_estado_btn24.ForeColor = System.Drawing.Color.White
        Me.gp_estado_btn24.Location = New System.Drawing.Point(284, 19)
        Me.gp_estado_btn24.Name = "gp_estado_btn24"
        Me.gp_estado_btn24.Size = New System.Drawing.Size(48, 48)
        Me.gp_estado_btn24.TabIndex = 78
        Me.gp_estado_btn24.UseVisualStyleBackColor = False
        '
        'gp_estado_btn22
        '
        Me.gp_estado_btn22.BackColor = System.Drawing.Color.Red
        Me.gp_estado_btn22.Cursor = System.Windows.Forms.Cursors.Hand
        Me.gp_estado_btn22.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gp_estado_btn22.ForeColor = System.Drawing.Color.White
        Me.gp_estado_btn22.Location = New System.Drawing.Point(115, 23)
        Me.gp_estado_btn22.Name = "gp_estado_btn22"
        Me.gp_estado_btn22.Size = New System.Drawing.Size(48, 48)
        Me.gp_estado_btn22.TabIndex = 68
        Me.gp_estado_btn22.UseVisualStyleBackColor = False
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(539, 475)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(331, 198)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 27
        Me.PictureBox1.TabStop = False
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(181, 514)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(152, 20)
        Me.Label11.TabIndex = 28
        Me.Label11.Text = "Seleciona la camara"
        '
        'BTN_1
        '
        Me.BTN_1.BackColor = System.Drawing.SystemColors.ControlLight
        Me.BTN_1.FlatAppearance.BorderSize = 0
        Me.BTN_1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BTN_1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BTN_1.Image = CType(resources.GetObject("BTN_1.Image"), System.Drawing.Image)
        Me.BTN_1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BTN_1.Location = New System.Drawing.Point(43, 561)
        Me.BTN_1.Name = "BTN_1"
        Me.BTN_1.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.BTN_1.Size = New System.Drawing.Size(66, 36)
        Me.BTN_1.TabIndex = 29
        Me.BTN_1.Text = " 1"
        Me.BTN_1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BTN_1.UseVisualStyleBackColor = False
        '
        'BTN_2
        '
        Me.BTN_2.BackColor = System.Drawing.SystemColors.ControlLight
        Me.BTN_2.FlatAppearance.BorderSize = 0
        Me.BTN_2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BTN_2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BTN_2.Image = CType(resources.GetObject("BTN_2.Image"), System.Drawing.Image)
        Me.BTN_2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BTN_2.Location = New System.Drawing.Point(151, 561)
        Me.BTN_2.Name = "BTN_2"
        Me.BTN_2.Size = New System.Drawing.Size(63, 36)
        Me.BTN_2.TabIndex = 30
        Me.BTN_2.Text = " 2"
        Me.BTN_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BTN_2.UseVisualStyleBackColor = False
        '
        'BTN_3
        '
        Me.BTN_3.BackColor = System.Drawing.SystemColors.ControlLight
        Me.BTN_3.FlatAppearance.BorderSize = 0
        Me.BTN_3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BTN_3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BTN_3.Image = CType(resources.GetObject("BTN_3.Image"), System.Drawing.Image)
        Me.BTN_3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BTN_3.Location = New System.Drawing.Point(283, 561)
        Me.BTN_3.Name = "BTN_3"
        Me.BTN_3.Size = New System.Drawing.Size(66, 36)
        Me.BTN_3.TabIndex = 31
        Me.BTN_3.Text = " 3"
        Me.BTN_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BTN_3.UseVisualStyleBackColor = False
        '
        'BTN_4
        '
        Me.BTN_4.BackColor = System.Drawing.SystemColors.ControlLight
        Me.BTN_4.FlatAppearance.BorderSize = 0
        Me.BTN_4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BTN_4.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BTN_4.Image = CType(resources.GetObject("BTN_4.Image"), System.Drawing.Image)
        Me.BTN_4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BTN_4.Location = New System.Drawing.Point(417, 561)
        Me.BTN_4.Name = "BTN_4"
        Me.BTN_4.Size = New System.Drawing.Size(68, 36)
        Me.BTN_4.TabIndex = 32
        Me.BTN_4.Text = "4"
        Me.BTN_4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BTN_4.UseVisualStyleBackColor = False
        Me.BTN_4.UseWaitCursor = True
        '
        'BTN_5
        '
        Me.BTN_5.BackColor = System.Drawing.SystemColors.ControlLight
        Me.BTN_5.FlatAppearance.BorderSize = 0
        Me.BTN_5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BTN_5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BTN_5.Image = CType(resources.GetObject("BTN_5.Image"), System.Drawing.Image)
        Me.BTN_5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BTN_5.Location = New System.Drawing.Point(43, 621)
        Me.BTN_5.Name = "BTN_5"
        Me.BTN_5.Size = New System.Drawing.Size(66, 36)
        Me.BTN_5.TabIndex = 33
        Me.BTN_5.Text = "5"
        Me.BTN_5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BTN_5.UseVisualStyleBackColor = False
        '
        'BTN_6
        '
        Me.BTN_6.BackColor = System.Drawing.SystemColors.ControlLight
        Me.BTN_6.FlatAppearance.BorderSize = 0
        Me.BTN_6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BTN_6.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BTN_6.Image = CType(resources.GetObject("BTN_6.Image"), System.Drawing.Image)
        Me.BTN_6.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BTN_6.Location = New System.Drawing.Point(151, 622)
        Me.BTN_6.Name = "BTN_6"
        Me.BTN_6.Size = New System.Drawing.Size(63, 36)
        Me.BTN_6.TabIndex = 34
        Me.BTN_6.Text = " 6"
        Me.BTN_6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BTN_6.UseVisualStyleBackColor = False
        '
        'BTN_7
        '
        Me.BTN_7.BackColor = System.Drawing.SystemColors.ControlLight
        Me.BTN_7.FlatAppearance.BorderSize = 0
        Me.BTN_7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BTN_7.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BTN_7.Image = CType(resources.GetObject("BTN_7.Image"), System.Drawing.Image)
        Me.BTN_7.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BTN_7.Location = New System.Drawing.Point(283, 621)
        Me.BTN_7.Name = "BTN_7"
        Me.BTN_7.Size = New System.Drawing.Size(66, 37)
        Me.BTN_7.TabIndex = 35
        Me.BTN_7.Text = " 7"
        Me.BTN_7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BTN_7.UseVisualStyleBackColor = False
        '
        'BTN_8
        '
        Me.BTN_8.BackColor = System.Drawing.SystemColors.ControlLight
        Me.BTN_8.FlatAppearance.BorderSize = 0
        Me.BTN_8.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BTN_8.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BTN_8.Image = CType(resources.GetObject("BTN_8.Image"), System.Drawing.Image)
        Me.BTN_8.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BTN_8.Location = New System.Drawing.Point(417, 622)
        Me.BTN_8.Name = "BTN_8"
        Me.BTN_8.Size = New System.Drawing.Size(68, 36)
        Me.BTN_8.TabIndex = 36
        Me.BTN_8.Text = " 8"
        Me.BTN_8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BTN_8.UseVisualStyleBackColor = False
        '
        'Automatico
        '
        Me.Automatico.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Automatico.FlatAppearance.BorderSize = 0
        Me.Automatico.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Automatico.Image = CType(resources.GetObject("Automatico.Image"), System.Drawing.Image)
        Me.Automatico.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Automatico.Location = New System.Drawing.Point(382, 507)
        Me.Automatico.Name = "Automatico"
        Me.Automatico.Size = New System.Drawing.Size(103, 36)
        Me.Automatico.TabIndex = 37
        Me.Automatico.Text = "Automatico"
        Me.Automatico.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Automatico.UseVisualStyleBackColor = False
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1370, 749)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.gp_btn_imprimir)
        Me.Controls.Add(Me.Automatico)
        Me.Controls.Add(Me.BTN_8)
        Me.Controls.Add(Me.BTN_7)
        Me.Controls.Add(Me.BTN_6)
        Me.Controls.Add(Me.BTN_5)
        Me.Controls.Add(Me.BTN_4)
        Me.Controls.Add(Me.BTN_3)
        Me.Controls.Add(Me.BTN_2)
        Me.Controls.Add(Me.BTN_1)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.gp_btn_ayuda)
        Me.Controls.Add(Me.GroupBox7)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.gp_btn_sc)
        Me.Controls.Add(Me.gp_btn_fp)
        Me.Controls.Add(Me.gp_btn_gg)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label3)
        Me.Name = "Form1"
        Me.Text = "Gestión del Parking"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox7.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox4.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents gp_input_matricula As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents gp_combobox As ComboBox
    Friend WithEvents Label6 As Label
    Friend WithEvents TextBox3 As TextBox
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents gp_btn_imprimir As Button
    Friend WithEvents Label8 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents gp_btn_gg As Button
    Friend WithEvents gp_btn_fp As Button
    Friend WithEvents gp_btn_sc As Button
    Friend WithEvents gp_datepicker_salida_fin As DateTimePicker
    Friend WithEvents gp_datepicker_salida_inicio As DateTimePicker
    Friend WithEvents gp_datepicker_entrada_fin As DateTimePicker
    Friend WithEvents Label9 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents GroupBox7 As GroupBox
    Friend WithEvents gp_estado_btn2 As Button
    Friend WithEvents gp_estado_btn1 As Button
    Friend WithEvents gp_btn_ayuda As Button
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents gp_estado_btn10 As Button
    Friend WithEvents gp_estado_btn5 As Button
    Friend WithEvents gp_estado_btn9 As Button
    Friend WithEvents gp_estado_btn8 As Button
    Friend WithEvents gp_estado_btn7 As Button
    Friend WithEvents gp_estado_btn6 As Button
    Friend WithEvents gp_estado_btn4 As Button
    Friend WithEvents gp_estado_btn3 As Button
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents gp_estado_btn20 As Button
    Friend WithEvents gp_estado_btn15 As Button
    Friend WithEvents gp_estado_btn19 As Button
    Friend WithEvents gp_estado_btn17 As Button
    Friend WithEvents gp_estado_btn16 As Button
    Friend WithEvents gp_estado_btn14 As Button
    Friend WithEvents gp_estado_btn13 As Button
    Friend WithEvents gp_estado_btn11 As Button
    Friend WithEvents GroupBox4 As GroupBox
    Friend WithEvents gp_estado_btn30 As Button
    Friend WithEvents gp_estado_btn25 As Button
    Friend WithEvents gp_estado_btn28 As Button
    Friend WithEvents gp_estado_btn27 As Button
    Friend WithEvents gp_estado_btn26 As Button
    Friend WithEvents gp_estado_btn24 As Button
    Friend WithEvents gp_estado_btn23 As Button
    Friend WithEvents gp_estado_btn22 As Button
    Friend WithEvents gp_estado_btn21 As Button
    Friend WithEvents gp_estado_btn12 As Button
    Friend WithEvents gp_estado_btn18 As Button
    Friend WithEvents gp_estado_btn29 As Button
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents Label11 As Label
    Friend WithEvents gp_datepicker_entrada_inicio As DateTimePicker
    Friend WithEvents BTN_1 As Button
    Friend WithEvents BTN_2 As Button
    Friend WithEvents BTN_3 As Button
    Friend WithEvents BTN_4 As Button
    Friend WithEvents BTN_5 As Button
    Friend WithEvents BTN_6 As Button
    Friend WithEvents BTN_7 As Button
    Friend WithEvents BTN_8 As Button
    Friend WithEvents Automatico As Button
    Friend WithEvents Button1 As Button
End Class
