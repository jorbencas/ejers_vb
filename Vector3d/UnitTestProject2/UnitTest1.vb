﻿Imports System.Text
Imports System.Math
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports Vector3d.Vector3D
<TestClass()> Public Class UnitTest1

    <TestMethod()> Public Sub TestMethod1()
        Dim x As Single = 2
        Dim y As Single = 2
        Dim z As Single = 3
        Dim prueba As Vector3d.Vector3D = New Vector3d.Vector3D(x, y, z)
        Dim resultado As Boolean = True

        Assert.AreEqual(resultado, prueba.Igual(prueba))
    End Sub

    <TestMethod()> Public Sub NormaMax()
        Dim x As Single = 3
        Dim y As Single = 3
        Dim z As Single = 3
        Dim prueba As Vector3d.Vector3D = New Vector3d.Vector3D(x, y, z)
        Dim resultado As Single = CSng((Sqrt(prueba.X ^ 2 + prueba.Y ^ 2 + prueba.Z ^ 2)))

        Assert.AreEqual(resultado, prueba.NormaMax())
    End Sub

    <TestMethod()> Public Sub Compare()
        Dim x As Single = 3
        Dim y As Single = 3
        Dim z As Single = 3
        Dim prueba As Vector3d.Vector3D = New Vector3d.Vector3D(x, y, z)
        Dim prueba2 As Vector3d.Vector3D = New Vector3d.Vector3D(x, y, z)

        Assert.AreEqual(0, prueba.CompareTo(prueba2))
    End Sub
End Class