Imports System
Imports Xunit

Namespace TestBurbujas
    Public Class UnitTest1
        Dim lista As List(Of Integer) = New List(Of Integer) From {2, 3, 4, 53, 1}
        Dim vector As VectorA = New VectorA(lista)

        <TestMethod()> Public Sub ComprobarForPrueba()
            Dim aux As List(Of Integer) = lista
            aux.Sort()
            Assert.AreEqual(vector.burbujaFor(), aux)
    End Class
End Namespace

