﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class GestionGastosvb
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.gg_input_infoconcepto = New System.Windows.Forms.TextBox()
        Me.gg_input_nfactura = New System.Windows.Forms.TextBox()
        Me.gg_btn_aceptar = New System.Windows.Forms.Button()
        Me.gg_btn_cancelar = New System.Windows.Forms.Button()
        Me.gg_datepicker = New System.Windows.Forms.DateTimePicker()
        Me.gp_btn_imprimir = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(254, 202)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(46, 16)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Fecha"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(254, 287)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(90, 16)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Nº de Factura"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(311, 33)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(211, 29)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Gestion de Gastos"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(254, 99)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(157, 16)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "Información de Concepto"
        '
        'gg_input_infoconcepto
        '
        Me.gg_input_infoconcepto.Location = New System.Drawing.Point(257, 145)
        Me.gg_input_infoconcepto.Name = "gg_input_infoconcepto"
        Me.gg_input_infoconcepto.Size = New System.Drawing.Size(314, 20)
        Me.gg_input_infoconcepto.TabIndex = 4
        '
        'gg_input_nfactura
        '
        Me.gg_input_nfactura.Location = New System.Drawing.Point(257, 329)
        Me.gg_input_nfactura.Name = "gg_input_nfactura"
        Me.gg_input_nfactura.Size = New System.Drawing.Size(314, 20)
        Me.gg_input_nfactura.TabIndex = 6
        '
        'gg_btn_aceptar
        '
        Me.gg_btn_aceptar.Location = New System.Drawing.Point(550, 396)
        Me.gg_btn_aceptar.Name = "gg_btn_aceptar"
        Me.gg_btn_aceptar.Size = New System.Drawing.Size(75, 23)
        Me.gg_btn_aceptar.TabIndex = 7
        Me.gg_btn_aceptar.Text = "Aceptar"
        Me.gg_btn_aceptar.UseVisualStyleBackColor = True
        '
        'gg_btn_cancelar
        '
        Me.gg_btn_cancelar.Location = New System.Drawing.Point(447, 396)
        Me.gg_btn_cancelar.Name = "gg_btn_cancelar"
        Me.gg_btn_cancelar.Size = New System.Drawing.Size(75, 23)
        Me.gg_btn_cancelar.TabIndex = 8
        Me.gg_btn_cancelar.Text = "Cancelar"
        Me.gg_btn_cancelar.UseVisualStyleBackColor = True
        '
        'gg_datepicker
        '
        Me.gg_datepicker.CustomFormat = "dd:MM:yyyy"
        Me.gg_datepicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.gg_datepicker.Location = New System.Drawing.Point(257, 236)
        Me.gg_datepicker.Name = "gg_datepicker"
        Me.gg_datepicker.Size = New System.Drawing.Size(314, 20)
        Me.gg_datepicker.TabIndex = 10
        '
        'gp_btn_imprimir
        '
        Me.gp_btn_imprimir.Location = New System.Drawing.Point(257, 396)
        Me.gp_btn_imprimir.Name = "gp_btn_imprimir"
        Me.gp_btn_imprimir.Size = New System.Drawing.Size(75, 23)
        Me.gp_btn_imprimir.TabIndex = 16
        Me.gp_btn_imprimir.Text = "Imprimir"
        Me.gp_btn_imprimir.UseVisualStyleBackColor = True
        '
        'GestionGastosvb
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.gp_btn_imprimir)
        Me.Controls.Add(Me.gg_datepicker)
        Me.Controls.Add(Me.gg_btn_cancelar)
        Me.Controls.Add(Me.gg_btn_aceptar)
        Me.Controls.Add(Me.gg_input_nfactura)
        Me.Controls.Add(Me.gg_input_infoconcepto)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Name = "GestionGastosvb"
        Me.Text = "GestionGastosvb"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents gg_input_infoconcepto As TextBox
    Friend WithEvents gg_input_nfactura As TextBox
    Friend WithEvents gg_btn_aceptar As Button
    Friend WithEvents gg_btn_cancelar As Button
    Friend WithEvents gg_datepicker As DateTimePicker
    Friend WithEvents gp_btn_imprimir As Button
End Class
