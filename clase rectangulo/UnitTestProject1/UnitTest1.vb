﻿Imports System.Text
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports clase_rectangulo

<TestClass()> Public Class UnitTest1

    <TestMethod()> Public Sub Cuadrado()
        Dim altura As Integer = 2
        Dim Anchura As Integer = 4
        Dim Rectangulas As Rectangulo = New Rectangulo(altura, Anchura)
        Dim loes As Boolean = False
        Assert.AreEqual(loes, Rectangulas.EsCuadrado)

    End Sub

    <TestMethod()> Public Sub nocuadrado()
        Dim altura As Integer = 4
        Dim Anchura As Integer = 4
        Dim Rectangulas As Rectangulo = New Rectangulo(altura, Anchura)
        Dim loes As Boolean = False
        Assert.AreNotEqual(loes, Rectangulas.EsCuadrado)

    End Sub

    <TestMethod()> Public Sub Area()
        Dim altura As Integer = 1
        Dim Anchura As Integer = 4
        Dim Rectangulos As Rectangulo = New Rectangulo(altura, Anchura)
        Dim area As Integer = 4

        Assert.AreEqual(area, Rectangulos.Area)

    End Sub

    <TestMethod()> Public Sub Perimetro()
        Dim altura As Integer = 2
        Dim Anchura As Integer = 4
        Dim Rectangulos As Rectangulo = New Rectangulo(altura, Anchura)
        Dim area As Double = 12
        Assert.AreEqual(area, Rectangulos.Perimetro)

    End Sub

    <TestMethod()> Public Sub Diagonal()
        Dim altura As Integer = 3
        Dim Anchura As Integer = 4
        Dim Rectangulos As Rectangulo = New Rectangulo(altura, Anchura)
        Dim area As Double = 5
        Assert.AreEqual(area, Rectangulos.Diagonal)

    End Sub
End Class