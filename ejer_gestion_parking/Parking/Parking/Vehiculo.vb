﻿Public Class Vehiculo
    Private tipo As String
    Private matricula As String
    Private Num_Plaza As Integer
    Private Num_Planta As Integer
    Public Sub New(tipo As String, matricula As String)
        Me.tipo = tipo
        Me.matricula = matricula
    End Sub

    Public Property Num_Plaza1 As Integer
        Get
            Return Num_Plaza
        End Get
        Set(value As Integer)
            Num_Plaza = value
        End Set
    End Property

    Public Property Num_Planta1 As Integer
        Get
            Return Num_Planta
        End Get
        Set(value As Integer)
            Num_Planta = value
        End Set
    End Property

    Public Function getTipo()
        Return Me.tipo
    End Function


    Public Function toString()
        Return Me.tipo & " " & Me.matricula
    End Function
End Class
