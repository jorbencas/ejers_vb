﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Fijar_Preciovb
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.fj_radiobutton_planta3 = New System.Windows.Forms.RadioButton()
        Me.fj_radiobutoon_planta2 = New System.Windows.Forms.RadioButton()
        Me.fj_rabiobutton_planta1 = New System.Windows.Forms.RadioButton()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.fj_chckbox_plaza = New System.Windows.Forms.CheckedListBox()
        Me.fj_btn_cancelr = New System.Windows.Forms.Button()
        Me.fj_btn_aceptar = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.fj_input_importe = New System.Windows.Forms.TextBox()
        Me.fj_datepicker = New System.Windows.Forms.DateTimePicker()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(330, 28)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(137, 29)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Fijar Precio"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.fj_radiobutton_planta3)
        Me.GroupBox1.Controls.Add(Me.fj_radiobutoon_planta2)
        Me.GroupBox1.Controls.Add(Me.fj_rabiobutton_planta1)
        Me.GroupBox1.Location = New System.Drawing.Point(160, 86)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(486, 81)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Planta"
        '
        'fj_radiobutton_planta3
        '
        Me.fj_radiobutton_planta3.AutoSize = True
        Me.fj_radiobutton_planta3.Location = New System.Drawing.Point(371, 34)
        Me.fj_radiobutton_planta3.Name = "fj_radiobutton_planta3"
        Me.fj_radiobutton_planta3.Size = New System.Drawing.Size(64, 17)
        Me.fj_radiobutton_planta3.TabIndex = 2
        Me.fj_radiobutton_planta3.TabStop = True
        Me.fj_radiobutton_planta3.Text = "Planta 3"
        Me.fj_radiobutton_planta3.UseVisualStyleBackColor = True
        '
        'fj_radiobutoon_planta2
        '
        Me.fj_radiobutoon_planta2.AutoSize = True
        Me.fj_radiobutoon_planta2.Location = New System.Drawing.Point(221, 34)
        Me.fj_radiobutoon_planta2.Name = "fj_radiobutoon_planta2"
        Me.fj_radiobutoon_planta2.Size = New System.Drawing.Size(64, 17)
        Me.fj_radiobutoon_planta2.TabIndex = 1
        Me.fj_radiobutoon_planta2.TabStop = True
        Me.fj_radiobutoon_planta2.Text = "Planta 2"
        Me.fj_radiobutoon_planta2.UseVisualStyleBackColor = True
        '
        'fj_rabiobutton_planta1
        '
        Me.fj_rabiobutton_planta1.AutoSize = True
        Me.fj_rabiobutton_planta1.Location = New System.Drawing.Point(41, 34)
        Me.fj_rabiobutton_planta1.Name = "fj_rabiobutton_planta1"
        Me.fj_rabiobutton_planta1.Size = New System.Drawing.Size(64, 17)
        Me.fj_rabiobutton_planta1.TabIndex = 0
        Me.fj_rabiobutton_planta1.TabStop = True
        Me.fj_rabiobutton_planta1.Text = "Planta 1"
        Me.fj_rabiobutton_planta1.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(129, 205)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(100, 16)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Eleguir la plaza"
        '
        'fj_chckbox_plaza
        '
        Me.fj_chckbox_plaza.FormattingEnabled = True
        Me.fj_chckbox_plaza.Items.AddRange(New Object() {"Plaza 1", "Plaza 2 ", "Plaza 3", "Plaza 4", "Plaza 5", "Plaza 6", "Plaza 7 ", "Plaza 8 ", "Plaza 9", "Plaza 10"})
        Me.fj_chckbox_plaza.Location = New System.Drawing.Point(210, 240)
        Me.fj_chckbox_plaza.Name = "fj_chckbox_plaza"
        Me.fj_chckbox_plaza.Size = New System.Drawing.Size(423, 49)
        Me.fj_chckbox_plaza.TabIndex = 3
        '
        'fj_btn_cancelr
        '
        Me.fj_btn_cancelr.Location = New System.Drawing.Point(300, 380)
        Me.fj_btn_cancelr.Name = "fj_btn_cancelr"
        Me.fj_btn_cancelr.Size = New System.Drawing.Size(75, 23)
        Me.fj_btn_cancelr.TabIndex = 4
        Me.fj_btn_cancelr.Text = "Cancelar"
        Me.fj_btn_cancelr.UseVisualStyleBackColor = True
        '
        'fj_btn_aceptar
        '
        Me.fj_btn_aceptar.Location = New System.Drawing.Point(453, 380)
        Me.fj_btn_aceptar.Name = "fj_btn_aceptar"
        Me.fj_btn_aceptar.Size = New System.Drawing.Size(75, 23)
        Me.fj_btn_aceptar.TabIndex = 5
        Me.fj_btn_aceptar.Text = "Aceptar"
        Me.fj_btn_aceptar.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(132, 304)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(53, 16)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "Importe"
        '
        'fj_input_importe
        '
        Me.fj_input_importe.Location = New System.Drawing.Point(180, 332)
        Me.fj_input_importe.Name = "fj_input_importe"
        Me.fj_input_importe.Size = New System.Drawing.Size(175, 20)
        Me.fj_input_importe.TabIndex = 7
        '
        'fj_datepicker
        '
        Me.fj_datepicker.Location = New System.Drawing.Point(493, 332)
        Me.fj_datepicker.Name = "fj_datepicker"
        Me.fj_datepicker.Size = New System.Drawing.Size(200, 20)
        Me.fj_datepicker.TabIndex = 8
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(453, 306)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(89, 13)
        Me.Label4.TabIndex = 9
        Me.Label4.Text = "Dia de la semana"
        '
        'Fijar_Preciovb
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.fj_datepicker)
        Me.Controls.Add(Me.fj_input_importe)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.fj_btn_aceptar)
        Me.Controls.Add(Me.fj_btn_cancelr)
        Me.Controls.Add(Me.fj_chckbox_plaza)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label1)
        Me.Name = "Fijar_Preciovb"
        Me.Text = "Fijar_Preciovb"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents fj_radiobutton_planta3 As RadioButton
    Friend WithEvents fj_radiobutoon_planta2 As RadioButton
    Friend WithEvents fj_rabiobutton_planta1 As RadioButton
    Friend WithEvents Label2 As Label
    Friend WithEvents fj_chckbox_plaza As CheckedListBox
    Friend WithEvents fj_btn_cancelr As Button
    Friend WithEvents fj_btn_aceptar As Button
    Friend WithEvents Label3 As Label
    Friend WithEvents fj_input_importe As TextBox
    Friend WithEvents fj_datepicker As DateTimePicker
    Friend WithEvents Label4 As Label
End Class
